FROM tomcat:8.5.46-jdk8-openjdk

RUN rm -Rf /usr/local/tomcat/webapps/ROOT
COPY ./target/*.war /usr/local/tomcat/webapps/ROOT.war